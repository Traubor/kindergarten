package pl.study.group;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import pl.study.model.Group;

/**
 * Created with IntelliJ IDEA.
 * User: avelife
 * Date: 11.01.14
 * Time: 14:25
 * To change this template use File | Settings | File Templates.
 */

public interface GroupRepository extends JpaRepository<Group,String>, JpaSpecificationExecutor {


    Group findById(Long groupId);
}
