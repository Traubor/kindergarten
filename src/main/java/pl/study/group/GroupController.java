package pl.study.group;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.study.model.Group;
import pl.study.model.IdWrapper;
import pl.study.model.Teacher;
import pl.study.teachers.TeacherRepository;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: avelife
 * Date: 07.01.14
 * Time: 21:56
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class GroupController {
    @Resource
    GroupRepository groupRepository;
    @Resource
    TeacherRepository teacherRepository;

    @RequestMapping(value = "/manageKindergarten/group/groups")
    @ModelAttribute("groups")
    public List<Group> getGroups() {
        List<Group> groups = groupRepository.findAll();
        return groups;
    }

    @RequestMapping(value = "/manageKindergarten/group/{groupId}", method = RequestMethod.GET)
    public String getGroupDetails(@PathVariable Long groupId, Model model) {

        Group group = groupRepository.findById(groupId);
        model.addAttribute("group", group);


        return "manageKindergarten/group/group_details";
    }

    @RequestMapping(value = "/manageKindergarten/group/add", method = RequestMethod.GET)
    public String add(Model model) {

        Group group = new Group();
        model.addAttribute("group", group);
        List<Teacher> teachers = teacherRepository.findAll();
        model.addAttribute("teachers", teachers);
        model.addAttribute("idWrapper", new IdWrapper());

        return "manageKindergarten/group/add_or_edit_group";
    }

    @RequestMapping(value = "/manageKindergarten/group/edit/{groupId}", method = RequestMethod.GET)
    public String edit(@PathVariable Long groupId, Model model) {

        Group group = groupRepository.findById(groupId);
        if (group != null) {
            model.addAttribute("group", group);
            List<Teacher> teachers = teacherRepository.findAll();
            model.addAttribute("teachers", teachers);
            model.addAttribute("idWrapper", new IdWrapper());
        }

        return "manageKindergarten/group/add_or_edit_group";
    }

    @RequestMapping(value = "/manageKindergarten/group/delete/{groupId}")
    private String delete(@PathVariable Long groupId) {

        Group group = groupRepository.findById(groupId);
        groupRepository.delete(group);
        return "redirect:/manageKindergarten/group/groups";
    }

    @RequestMapping(value = "/manageKindergarten/group/addOrUpdate", method = RequestMethod.POST)
    public String addOrUpdateGroup(@ModelAttribute Group group, @ModelAttribute IdWrapper idWrapper) {

        Teacher teacher = teacherRepository.findById(idWrapper.getWrappedId());
        Group foundGroup = groupRepository.findById(group.getId());

        if (foundGroup == null) {
            group.setTeacher(teacher);
            groupRepository.saveAndFlush(group);
        } else {
            foundGroup.setTeacher(teacher);
            foundGroup.setName(group.getName());
            groupRepository.saveAndFlush(foundGroup);
        }

        return "redirect:/manageKindergarten/group/groups";
    }

}
