package pl.study.model;

/**
 * Created with IntelliJ IDEA.
 * User: losiu
 * Date: 18.01.2014
 * Time: 20:18
 * To change this template use File | Settings | File Templates.
 */
public interface HasAddress {

    void setAddress(Address address);

    Address getAddress();
}
