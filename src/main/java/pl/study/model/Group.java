package pl.study.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: avelife
 * Date: 09.12.13
 * Time: 23:12
 * To change this template use File | Settings | File Templates.
 */

@Entity(name = "kindergarten_group")
public class Group implements Serializable {


    @Id
    @GeneratedValue
    @Column(name = "group_id")
    private Long id;

    @Column(unique = true)
    private String name;

    @OneToMany(mappedBy = "group", targetEntity = Child.class, fetch = FetchType.LAZY)
    private Set<Child> children;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "teacher_id")
    private Teacher teacher;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public Set<Child> getChildren() {
        return children;
    }

    public void setChildren(Set<Child> children) {
        this.children = children;
    }

}
