package pl.study.model;

import org.joda.time.DateTime;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: avelife
 * Date: 09.12.13
 * Time: 23:11
 * To change this template use File | Settings | File Templates.
 */
//TODO: Check if getters and setters are actually needed
@Entity
public class Child implements Serializable, HasAddress {

    @Id
    @GeneratedValue
    @Column(name = "child_id")
    private Long id;

    private String name;

    private String surname;

    private String identifier;

    @Column(name = "is_registered")
    private boolean isRegistered;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "group_id", nullable = true, updatable = false)
    private Group group;

    @ManyToMany(mappedBy = "children", fetch = FetchType.LAZY, targetEntity = Parent.class, cascade = CascadeType.REMOVE)
    private Set<Parent> parents;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE, targetEntity = Address.class)
    @JoinColumn(
            name = "address_id", unique = false, nullable = false, updatable = false)
    private Address address;

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Set<Parent> getParents() {
        return parents;
    }

    public void setParents(Set<Parent> parents) {
        this.parents = parents;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    @Override
    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public Address getAddress() {
        return address;
    }

    public boolean isRegistered() {
        return isRegistered;
    }

    public void setRegistered(boolean registered) {
        isRegistered = registered;
    }
}
