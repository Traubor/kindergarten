package pl.study.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Created with IntelliJ IDEA.
 * User: avelife
 * Date: 11.12.13
 * Time: 20:51
 * To change this template use File | Settings | File Templates.
 */
@Entity
@DiscriminatorValue("ROLE_ADMIN")
public class Admin extends Account {

    public Admin(){

    }

    public Admin(String admin, String admin1, String role_admin) {
        super(admin,admin1,role_admin);

    }
}
