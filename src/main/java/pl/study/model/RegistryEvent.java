package pl.study.model;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.joda.time.DateTime;
import pl.study.register.RegistryEventType;

import javax.persistence.*;
import java.sql.Timestamp;


@Entity(name = "registry")
public class RegistryEvent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "child_id")
    private long childId;

    @Column(name = "event_date")
    private Timestamp eventDate;

    @Column(name = "event_type")
    @Enumerated(EnumType.STRING)
    private RegistryEventType eventType;

    public RegistryEvent() {

    }

    public RegistryEvent(Long childId, RegistryEventType eventType) {
        this.childId = childId;
        this.eventType = eventType;
        this.eventDate = new Timestamp(new DateTime().getMillis());
    }

    public RegistryEvent(Long childId, RegistryEventType eventType, Timestamp eventDate) {
        this.childId = childId;
        this.eventType = eventType;
        this.eventDate = eventDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Timestamp getEventDate() {
        return eventDate;
    }

    public void setEventDate(Timestamp eventDate) {
        this.eventDate = eventDate;
    }

    public RegistryEventType getEventType() {
        return eventType;
    }

    public void setEventType(RegistryEventType eventType) {
        this.eventType = eventType;
    }
}
