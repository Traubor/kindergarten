package pl.study.model;

import javax.persistence.*;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: avelife
 * Date: 11.12.13
 * Time: 20:15
 * To change this template use File | Settings | File Templates.
 */
@Entity
@DiscriminatorValue("ROLE_PARENT")
public class Parent extends Account implements HasAddress {

    public Parent(String email, String password, String role) {
        super(email, password, role);
    }

    public Parent() {

    }

    private String name;

    private String surname;

    @Column(name = "identity_number")
    private Long identityNumber;

    @Column(name = "phone_number")
    private String phoneNumber;

    @ManyToMany
    @JoinTable(name = "parent_child" ,joinColumns = {@JoinColumn(name = "parent_id")}, inverseJoinColumns = {@JoinColumn(name = "child_id")})
    private Set<Child> children;


    @OneToOne(cascade = CascadeType.REMOVE, targetEntity = Address.class, fetch = FetchType.EAGER)
    @JoinColumn(
            name = "address_id", unique = false, nullable = true, updatable = false)
    private Address address;

    @Override
    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public Address getAddress() {
        return address;
    }

    public Long getIdentityNumber() {
        return identityNumber;
    }

    public void setIdentityNumber(Long identityNumber) {
        this.identityNumber = identityNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Set<Child> getChildren() {
        return children;
    }

    public void setChildren(Set<Child> children) {
        this.children = children;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
}
