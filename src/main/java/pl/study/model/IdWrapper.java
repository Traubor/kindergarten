package pl.study.model;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: losiu
 * Date: 19.01.2014
 * Time: 13:20
 * To change this template use File | Settings | File Templates.
 */
public class IdWrapper implements Serializable {

    public IdWrapper(){
    }

    public IdWrapper(Long id){
        wrappedId = id;
    }

    private Long wrappedId;

    public Long getWrappedId() {
        return wrappedId;
    }

    public void setWrappedId(Long id) {
        this.wrappedId = id;
    }
}
