package pl.study.model;

import org.codehaus.jackson.annotate.JsonProperty;
import pl.study.children.RegistryEventDTO;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: losiu
 * Date: 09.02.2014
 * Time: 21:52
 * To change this template use File | Settings | File Templates.
 */
public class RegistryEvents {

    @JsonProperty("registryList")
    private List<RegistryEventDTO> registryEvents;

    public List<RegistryEventDTO> getRegistryEvents() {
        return registryEvents;
    }

    public void setRegistryEvents(List<RegistryEventDTO> registryEvents) {
        this.registryEvents = registryEvents;
    }
}
