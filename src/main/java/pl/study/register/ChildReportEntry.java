package pl.study.register;

import java.sql.Timestamp;

/**
 * Created with IntelliJ IDEA.
 * User: losiu
 * Date: 02.02.2014
 * Time: 23:38
 * To change this template use File | Settings | File Templates.
 */


public class ChildReportEntry {

    private Timestamp date;
    private long msInKindergarten;
    private long childId;

    public ChildReportEntry(Timestamp date, long msInKindergarten, long childId) {
        this.date = date;
        this.msInKindergarten = msInKindergarten;
        this.childId = childId;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public long getMsInKindergarten() {
        return msInKindergarten;
    }

    public void setMsInKindergarten(long msInKindergarten) {
        this.msInKindergarten = msInKindergarten;
    }

    public long getChildId() {
        return childId;
    }

    public void setChildId(long childId) {
        this.childId = childId;
    }
}
