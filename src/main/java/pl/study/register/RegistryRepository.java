package pl.study.register;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import pl.study.model.RegistryEvent;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: avelife
 * Date: 14.12.13
 * Time: 17:22
 * To change this template use File | Settings | File Templates.
 */
public interface RegistryRepository extends JpaRepository<RegistryEvent, String>, JpaSpecificationExecutor {

    @Query(value = "SELECT sample_function(?1)", nativeQuery = true)
    public String someQuery(long childId);

    @Query(value = "SELECT date_trunc('day', event_date) AS \"day\", round(cast(sum(s.sum_sec)/(60*60) as numeric), 1) \n" +
            "from registry r \n" +
            "join (\n" +
            "    SELECT id,\n" +
            "\tCASE WHEN lag(event_type) OVER w  = 'REGISTER' THEN\n" +
            "\t    EXTRACT(EPOCH FROM event_date - lag(event_date) OVER w)\n" +
            "\tELSE\n" +
            "\t    0\n" +
            "\tEND as sum_sec\n" +
            "\t    FROM   registry r\n" +
            "\t    WINDOW w AS (PARTITION BY child_id ORDER BY event_date)\n" +
            "    ) as s\n" +
            "on r.id = s.id    \n" +
            "where child_id=?1 and extract(month from event_date) = extract(month from current_timestamp)\n" +
            "group by \"day\";", nativeQuery = true)
    List<Object[]> getMothReportData(Long childId);
}
