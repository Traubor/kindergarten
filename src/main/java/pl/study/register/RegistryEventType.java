package pl.study.register;

/**
 * Created with IntelliJ IDEA.
 * User: losiu
 * Date: 24.01.2014
 * Time: 22:01
 * To change this template use File | Settings | File Templates.
 */
public enum RegistryEventType {
    REGISTER, UNREGISTER;
}
