package pl.study.address;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import pl.study.model.Address;

/**
 * Created with IntelliJ IDEA.
 * User: losiu
 * Date: 18.01.2014
 * Time: 12:23
 * To change this template use File | Settings | File Templates.
 */
public interface AddressRepository extends JpaRepository<Address, String>, JpaSpecificationExecutor {

    Address findByAddressId(Long addressId);
}
