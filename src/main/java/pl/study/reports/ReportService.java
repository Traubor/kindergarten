package pl.study.reports;

import com.lowagie.text.DocumentException;
import org.apache.commons.io.IOUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import pl.study.children.ChildRepository;
import pl.study.model.Child;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Locale;

@Controller
public class ReportService {

    @Resource
    ChildRepository childRepository;
    @Autowired
    ReportGenerator reportGenerator;
    @Value("${report.dir}")
    String reportDirectory;

    @Value("${report.language}")
    String reportLanguage;

    //@Scheduled(cron = "* 0,30 * * * *")
    private void generateMonthlyReports() throws IOException, DocumentException {
        System.out.println("Generating reports...");
        List<Child> children = childRepository.findAll();
        DateTimeFormatter fmt = DateTimeFormat.forPattern("MM.yyyy");
        for (Child child : children) {
            String newDirectory = String.format(reportDirectory, fmt.print(new DateTime()), child.getGroup().getName());
            new File(newDirectory).mkdirs();
            reportGenerator.generateReport(child, newDirectory, Locale.forLanguageTag(reportLanguage));
        }
        System.out.println("Finished.");
    }

    @RequestMapping(value = "/manageKindergarten/report/{child_id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<byte[]> generateReportForChild(@PathVariable("child_id") Long childId, Locale locale) throws IOException, DocumentException {

        Child child = childRepository.findById(childId);
        reportGenerator.generateReport(child, null, locale);

        File file = new File(ReportGenerator.TMEP_PDF);
        InputStream inputStream = new FileInputStream(file);
        byte[] contents = IOUtils.toByteArray(inputStream);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/pdf"));
        String filename = String.format("%1s_%2s.pdf", child.getName(), child.getSurname());
        headers.setContentDispositionFormData(filename, filename);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        ResponseEntity<byte[]> response = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
        return response;
    }

}
