package pl.study.reports;

import com.google.common.collect.Lists;
import com.lowagie.text.*;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.stereotype.Component;
import pl.study.model.Child;
import pl.study.register.RegistryRepository;

import java.awt.*;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.Locale;
import java.util.List;

@Component
public class ReportGenerator implements MessageSourceAware {

    public static final String TMEP_PDF = "temp.pdf";
    private static Double rate = 15.0;
    @Value("${report.currency}")
    String currency;
    @Autowired
    private RegistryRepository registryRepository;
    private MessageSource messageSource;
    private Locale locale;

    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    public void generateReport(Child child, String reportDirectory, Locale locale) throws DocumentException, IOException {

        this.locale = locale;
        Document document = new Document(PageSize.A4, 30f, 30f, 160f, 30f);
        FileOutputStream fos = null;
        if (reportDirectory == null) {
            fos = new FileOutputStream(TMEP_PDF);
        } else {
            fos = new FileOutputStream(reportDirectory + String.format("%1s_%2s.pdf", child.getName(), child.getSurname()));
        }
        PdfWriter writer = PdfWriter.getInstance(document, fos);

        generateHeaderAndFooter(document, child);
        document.open();
        generateMainContent(document, child);
        document.close();
        fos.close();
    }

    private void generateHeaderAndFooter(Document document, Child child) throws IOException, DocumentException {

        String path = getClass().getResource("/pdf_header.jpg").getPath();
        Image logo = Image.getInstance(path);
        logo.setAlignment(Image.HEADER);
        logo.scaleAbsoluteHeight(40);
        logo.scaleAbsoluteWidth(400);
        logo.scalePercent(60);
        Chunk chunk = new Chunk(logo, 0, -65);
        HeaderFooter header = new HeaderFooter(new Phrase(chunk), false);
        header.setAlignment(Element.ALIGN_CENTER);
        header.setBorder(Rectangle.NO_BORDER);
        document.setHeader(header);

        path = getClass().getResource("/pdf_footer.jpg").getPath();
        logo = Image.getInstance(path);
        logo.setAlignment(Element.ALIGN_RIGHT);
        logo.scalePercent(40);
        chunk = new Chunk(logo, 0, 0);
        HeaderFooter footer = new HeaderFooter(new Phrase(chunk), false);
        footer.setAlignment(Element.ALIGN_RIGHT);
        footer.setBorder(Rectangle.NO_BORDER);
        document.setFooter(footer);
    }

    private void generateMainContent(Document document, Child child) throws DocumentException, IOException {

        List<ReportEntry> reportEnties = getReportEntries(child.getId());

        BaseFont bf = BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, BaseFont.EMBEDDED);

        String name = messageSource.getMessage("report.child.name",
                new Object[]{child.getName()}, locale);
        Paragraph p = new Paragraph(name, new Font(bf, 18));
        document.add(p);

        String surname = messageSource.getMessage("report.child.surname",
                new Object[]{child.getSurname()}, locale);
        p = new Paragraph(surname, new Font(bf, 18));
        p.setSpacingAfter(40);
        document.add(p);

        Font tableTitleFont = new Font(bf, 16, Font.BOLD);
        String tableTitle = messageSource.getMessage("report.presence.table.title",
                new Object[]{}, locale);
        p = new Paragraph(tableTitle, tableTitleFont);
        p.setAlignment(Element.ALIGN_CENTER);
        p.setSpacingAfter(15);
        document.add(p);
        PdfPTable table = new PdfPTable(4);
        Font tableFont = new Font(bf, 13);
        table.setWidths(new int[]{60, 60, 100, 70});
        String tableDay = messageSource.getMessage("report.presence.table.day",
                new Object[]{}, locale);
        PdfPCell c1 = new PdfPCell(new Phrase(tableDay, tableFont));
        c1.setHorizontalAlignment(Element.ALIGN_LEFT);

        // background color
        c1.setBackgroundColor(Color.ORANGE);

        // padding
        c1.setPaddingBottom(5f);

        // border
        c1.setBorder(1);
        c1.setBorderWidthBottom(2f);
        table.addCell(c1);

        String tableHours = messageSource.getMessage("report.presence.table.hours",
                new Object[]{}, locale);
        c1 = new PdfPCell(new Phrase(tableHours, tableFont));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);

        // background color
        c1.setBackgroundColor(Color.ORANGE);

        // padding
        c1.setPaddingBottom(5f);

        // border
        c1.setBorder(1);
        c1.setBorderWidthBottom(2f);
        table.addCell(c1);

        String tableRate = messageSource.getMessage("report.presence.table.rate",
                new Object[]{}, locale);
        c1 = new PdfPCell(new Phrase(tableRate, tableFont));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);

        // background color
        c1.setBackgroundColor(Color.ORANGE);

        // padding
        c1.setPaddingBottom(5f);

        // border
        c1.setBorder(1);
        c1.setBorderWidthBottom(2f);
        table.addCell(c1);

        String tableCost = messageSource.getMessage("report.presence.table.cost",
                new Object[]{}, locale);
        c1 = new PdfPCell(new Phrase(tableCost, tableFont));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        // background color
        c1.setBackgroundColor(Color.ORANGE);

        // padding
        c1.setPaddingBottom(5f);

        // border
        c1.setBorder(1);
        c1.setBorderWidthBottom(2f);
        table.addCell(c1);

        Double sum = 0.0;
        for (ReportEntry entry : reportEnties) {
            DateTimeFormatter fmt = DateTimeFormat.forPattern("dd.MM.yyyy");
            table.addCell(fmt.print(entry.getDay()));
            Double hours = entry.getHoursInKindergarten();
            table.addCell(hours.toString());
            table.addCell(String.format("%1s %2s", rate.toString(), currency));
            Double payment = hours * rate;
            table.addCell(String.format("%1s %2s", payment.toString(), currency));
            sum += payment;
        }
        String tableSum = messageSource.getMessage("report.presence.table.sum",
                new Object[]{sum, currency}, locale);
        PdfPCell sumCell = new PdfPCell(new Phrase(tableSum, tableFont));
        sumCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        sumCell.setColspan(4);
        table.addCell(sumCell);


        document.add(table);
    }

    private List<ReportEntry> getReportEntries(Long childId) {

        List<Object[]> results = registryRepository.getMothReportData(childId);
        List<ReportEntry> reportEnties = Lists.newArrayList();
        for (Object[] result : results) {
            reportEnties.add(new ReportEntry((Timestamp) result[0], (BigDecimal) result[1]));
        }
        return reportEnties;
    }
}
