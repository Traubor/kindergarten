package pl.study.reports;

import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * Created with IntelliJ IDEA.
 * User: losiu
 * Date: 07.02.2014
 * Time: 10:52
 * To change this template use File | Settings | File Templates.
 */
public class ReportEntry {

    private DateTime day;
    private double hoursInKindergarten;

    public ReportEntry(Timestamp day, BigDecimal hoursInKindergarten) {
        this.day = new DateTime(day);
        this.hoursInKindergarten = hoursInKindergarten.doubleValue();
    }

    public DateTime getDay() {
        return day;
    }

    public double getHoursInKindergarten() {
        return hoursInKindergarten;
    }
}
