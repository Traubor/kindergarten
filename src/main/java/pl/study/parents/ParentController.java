package pl.study.parents;

import com.google.common.collect.Sets;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.study.address.AddressRepository;
import pl.study.children.ChildRepository;
import pl.study.model.Address;
import pl.study.model.Child;
import pl.study.model.IdWrapper;
import pl.study.model.Parent;

import javax.annotation.Resource;
import javax.inject.Inject;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: losiu
 * Date: 17.01.2014
 * Time: 14:18
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class ParentController {

    @Inject
    private PasswordEncoder passwordEncoder;

    @Resource
    ParentRepository parentRepository;

    @Resource
    AddressRepository addressRepository;

    @Resource
    ChildRepository childRepository;

    @RequestMapping(value = "/manageKindergarten/parent/parents")
    @ModelAttribute("parents")
    private List<Parent> getparents() {

        List<Parent> parents = parentRepository.findAll();
        return parents;
    }

    @RequestMapping(value = "/manageKindergarten/parent/{parentId}")
    private String getDetail(@PathVariable Long parentId, Model model) {

        Parent parent = parentRepository.findById(parentId);
        if (parent != null) {
            model.addAttribute("parent", parent);
            model.addAttribute("address", parent.getAddress());
        }
        return "/manageKindergarten/parent/parent_details";
    }

    @RequestMapping(value = "/manageKindergarten/parent/delete/{parentId},{childId}")
    private String delete(@PathVariable Long parentId, @PathVariable Long childId) {

        Parent parent = parentRepository.findById(parentId);
        parentRepository.delete(parent);
        return "redirect:/manageKindergarten/child/"+childId;
    }

    @RequestMapping(value = "/manageKindergarten/parent/edit/{parentId},{childId}")
    private String edit(@PathVariable Long parentId, @PathVariable Long childId, Model model) {

        Parent parent = parentRepository.findById(parentId);
        if (parent != null) {
            model.addAttribute("parent", parent);
            model.addAttribute("address", parent.getAddress());
            model.addAttribute("childId", new IdWrapper(childId));
        }
        return "/manageKindergarten/parent/add_or_edit_parent";
    }

    @RequestMapping(value = "/manageKindergarten/parent/add/{childId}")
    private String add(@PathVariable Long childId, Model model) {

        Parent parent = new Parent();
        model.addAttribute("parent", parent);
        model.addAttribute("childId", new IdWrapper(childId));

        Address address = new Address();
        model.addAttribute("address", address);

        return "/manageKindergarten/parent/add_or_edit_parent";
    }

    @RequestMapping(value = "/manageKindergarten/parent/addOrUpdate", method = RequestMethod.POST)
    public String addOrUpdateParent(@ModelAttribute Parent parent, @ModelAttribute Address address, @ModelAttribute IdWrapper childId) {

        saveOrUpdateAddress(address);
        saveOrUpdateParent(parent, childId, address);

        return "redirect:/manageKindergarten/child/"+childId.getWrappedId();
    }

    private void saveOrUpdateParent(Parent parent, IdWrapper wrappedChildId, Address address) {
        Parent foundParent = parentRepository.findById(parent.getId());
        if (foundParent == null) {
            Set<Child> children = Sets.newHashSet();
            children.add(childRepository.findById(wrappedChildId.getWrappedId()));
            parent.setChildren(children);
            parent.setPassword(passwordEncoder.encode(parent.getPassword()));
            parent.setAddress(address);
            parent.setRole("ROLE_PARENT");
            parentRepository.saveAndFlush(parent);
        } else {
            foundParent.setName(parent.getName());
            foundParent.setSurname(parent.getSurname());
            foundParent.setIdentityNumber(parent.getIdentityNumber());
            foundParent.setPhoneNumber(parent.getPhoneNumber());
            foundParent.setAddress(address);
            foundParent.setEmail(parent.getEmail());
            Set<Child> children = foundParent.getChildren();
            children.add(childRepository.findById(wrappedChildId.getWrappedId()));
            foundParent.setChildren(children);
            parentRepository.saveAndFlush(foundParent);
        }
    }

    private void saveOrUpdateAddress(Address address) {
        Address foundAddress = addressRepository.findByAddressId(address.getAddressId());
        if (foundAddress == null) {
            addressRepository.saveAndFlush(address);
        } else {
            foundAddress.setStreetName(address.getStreetName());
            foundAddress.setHouseNumber(address.getHouseNumber());
            foundAddress.setFlatNumber(address.getFlatNumber());
            foundAddress.setPostalCode(address.getPostalCode());
            addressRepository.saveAndFlush(foundAddress);
        }
    }
}
