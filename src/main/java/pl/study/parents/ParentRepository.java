package pl.study.parents;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import pl.study.model.Parent;

/**
 * Created with IntelliJ IDEA.
 * User: losiu
 * Date: 17.01.2014
 * Time: 14:19
 * To change this template use File | Settings | File Templates.
 */
public interface ParentRepository extends JpaRepository<Parent, String>, JpaSpecificationExecutor {


    Parent findById(Long parentId);
}
