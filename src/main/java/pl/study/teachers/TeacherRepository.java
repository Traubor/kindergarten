package pl.study.teachers;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import pl.study.model.Teacher;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: losiu
 * Date: 17.01.2014
 * Time: 14:19
 * To change this template use File | Settings | File Templates.
 */
public interface TeacherRepository extends JpaRepository<Teacher,String>, JpaSpecificationExecutor {


    Teacher findById(Long teacherId);
}
