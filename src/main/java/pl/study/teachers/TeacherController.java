package pl.study.teachers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.study.address.AddressRepository;
import pl.study.group.GroupRepository;
import pl.study.model.Address;
import pl.study.model.Group;
import pl.study.model.Teacher;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: losiu
 * Date: 17.01.2014
 * Time: 14:18
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class TeacherController {

    @Resource
    TeacherRepository teacherRepository;

    @Resource
    GroupRepository groupRepository;

    @Resource
    AddressRepository addressRepository;

    @RequestMapping(value = "/manageKindergarten/teacher/teachers")
    @ModelAttribute("teachers")
    private List<Teacher> getTeachers() {

        List<Teacher> teachers = teacherRepository.findAll();
        return teachers;
    }

    @RequestMapping(value = "/manageKindergarten/teacher/{teacherId}")
    private String getDetail(@PathVariable Long teacherId, Model model) {

        Teacher teacher = teacherRepository.findById(teacherId);
        if (teacher != null) {
            model.addAttribute("teacher", teacher);
            model.addAttribute("address", teacher.getAddress());
        }
        return "/manageKindergarten/teacher/teacher_details";
    }

    @RequestMapping(value = "/manageKindergarten/teacher/delete/{teacherId}")
    private String delete(@PathVariable Long teacherId) {

        Teacher teacher = teacherRepository.findById(teacherId);

        Group group = groupRepository.findById(teacher.getGroup().getId());
        group.setTeacher(null);
        groupRepository.saveAndFlush(group);
        teacherRepository.delete(teacher);
        return "redirect:/manageKindergarten/teacher/teachers";
    }

    @RequestMapping(value = "/manageKindergarten/teacher/edit/{teacherId}")
    private String edit(@PathVariable Long teacherId, Model model) {

        Teacher teacher = teacherRepository.findById(teacherId);
        if (teacher != null) {
            model.addAttribute("teacher", teacher);
            model.addAttribute("address", teacher.getAddress());
        }
        return "/manageKindergarten/teacher/add_or_edit_teacher";
    }

    @RequestMapping(value = "/manageKindergarten/teacher/add")
    private String add(Model model) {

        Teacher teacher = new Teacher();
        model.addAttribute("teacher", teacher);

        Address address = new Address();
        model.addAttribute("address", address);

        return "/manageKindergarten/teacher/add_or_edit_teacher";
    }

    @RequestMapping(value = "/manageKindergarten/teacher/addOrUpdate", method = RequestMethod.POST)
    public String addOrUpdateTeacher(@ModelAttribute Teacher teacher, @ModelAttribute Address address) {

        saveOrUpdateAddress(address);
        saveOrUpdateTeacher(teacher, address);

        return "redirect:/manageKindergarten/teacher/teachers";
    }

    private void saveOrUpdateTeacher(Teacher teacher, Address address) {
        Teacher foundTeacher = teacherRepository.findById(teacher.getId());
        if (foundTeacher == null) {
            teacher.setAddress(address);
            teacherRepository.saveAndFlush(teacher);
        } else {
            foundTeacher.setName(teacher.getName());
            foundTeacher.setSurname(teacher.getSurname());
            foundTeacher.setIdentityNumber(teacher.getIdentityNumber());
            foundTeacher.setPhoneNumber(teacher.getPhoneNumber());
            foundTeacher.setAddress(address);
            foundTeacher.setEmailAddress(teacher.getEmailAddress());
            teacherRepository.saveAndFlush(foundTeacher);
        }
    }

    private void saveOrUpdateAddress(Address address) {
        Address foundAddress = addressRepository.findByAddressId(address.getAddressId());
        if (foundAddress == null) {
            addressRepository.saveAndFlush(address);
        } else {
            foundAddress.setStreetName(address.getStreetName());
            foundAddress.setHouseNumber(address.getHouseNumber());
            foundAddress.setFlatNumber(address.getFlatNumber());
            foundAddress.setPostalCode(address.getPostalCode());
            addressRepository.saveAndFlush(foundAddress);
        }
    }
}
