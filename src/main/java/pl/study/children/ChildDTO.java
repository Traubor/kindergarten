package pl.study.children;

/**
 * Created with IntelliJ IDEA.
 * User: losiu
 * Date: 24.01.2014
 * Time: 20:19
 * To change this template use File | Settings | File Templates.
 */
public class ChildDTO {

    private Long id;

    private String name;

    private String surname;

    private String groupName;

    private String identifier;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }
}
