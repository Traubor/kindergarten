package pl.study.children;

import org.codehaus.jackson.annotate.JsonProperty;


public class RegistryEventDTO {


    @JsonProperty("childId")

    private long childId;
    @JsonProperty("eventDate")

    private String eventDate;
    @JsonProperty("eventType")

    private String eventType;

    public long getChildId() {
        return childId;
    }

    public void setChildId(long childId) {
        this.childId = childId;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }
}
