package pl.study.children;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.cdi.JpaRepositoryExtension;
import org.springframework.stereotype.Repository;
import pl.study.model.Child;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: avelife
 * Date: 14.12.13
 * Time: 17:22
 * To change this template use File | Settings | File Templates.
 */
public interface ChildRepository extends JpaRepository<Child,String>, JpaSpecificationExecutor {


    public Child findById(Long childId);

    public Child findByIdentifier(String identifier);

    @Query("SELECT c FROM Child c")
    public List<Child> getRawChildren();
}
