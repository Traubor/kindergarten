package pl.study.children;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.thymeleaf.context.WebContext;
import pl.study.address.AddressRepository;
import pl.study.group.GroupRepository;
import pl.study.model.*;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: avelife
 * Date: 08.12.13
 * Time: 19:42
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class ChildrenController {
    @Resource
    ChildRepository childRepository;

    @Resource
    AddressRepository addressRepository;

    @Resource
    GroupRepository groupRepository;

    @RequestMapping(value = "/manageKindergarten/child/children", produces = "text/html; charset=utf-8")

    @ModelAttribute("children")
    private List<Child> getChildren() {

        List<Child> children = childRepository.findAll();
        return children;
    }

    @RequestMapping(value = "/manageKindergarten/child/{childId}")
    private String getDetail(@PathVariable Long childId, Model model) {

        Child child = childRepository.findById(childId);
        if (child != null) {
            model.addAttribute("child", child);
            model.addAttribute("address", child.getAddress());
        }
        return "/manageKindergarten/child/child_details";
    }

    @RequestMapping(value = "/manageKindergarten/child/delete/{childId}")
    private String delete(@PathVariable Long childId) {

        Child child = childRepository.findById(childId);
        childRepository.delete(child);
        return "redirect:/manageKindergarten/child/children";
    }

    @RequestMapping(value = "/manageKindergarten/child/edit/{childId}")
    private String edit(@PathVariable Long childId, Model model) {

        Child child = childRepository.findById(childId);
        if (child != null) {
            model.addAttribute("child", child);
            model.addAttribute("address", child.getAddress());
            model.addAttribute("groups", groupRepository.findAll());
            model.addAttribute("idWrapper", new IdWrapper());
        }
        return "/manageKindergarten/child/add_or_edit_child";
    }

    @RequestMapping(value = "/manageKindergarten/child/add")
    private String add(Model model) {

        Child child = new Child();
        model.addAttribute("child", child);
        Address address = new Address();
        model.addAttribute("address", address);
        model.addAttribute("groups", groupRepository.findAll());
        model.addAttribute("idWrapper", new IdWrapper());

        return "/manageKindergarten/child/add_or_edit_child";
    }

    @RequestMapping(value = "/manageKindergarten/child/addOrUpdate", method = RequestMethod.POST)
    public String addOrUpdateChild(@ModelAttribute Child child, @ModelAttribute Address address, @ModelAttribute IdWrapper idWrapper) {

        saveOrUpdateAddress(address);
        saveOrUpdateChild(child, address, idWrapper);

        return "redirect:/manageKindergarten/child/children";
    }

    private void saveOrUpdateChild(Child child, Address address, IdWrapper idWrapper) {
        Child foundChild = childRepository.findById(child.getId());
        Group group = groupRepository.findById(idWrapper.getWrappedId());
        if (foundChild == null) {
            child.setAddress(address);
            child.setGroup(group);
            childRepository.saveAndFlush(child);
            // identifier uses child.id so it can be generated after saving
            child.setIdentifier(generateIdentifier(child));
            childRepository.saveAndFlush(child);
        } else{
            foundChild.setName(child.getName());
            foundChild.setSurname(child.getSurname());
            foundChild.setAddress(address);
            foundChild.setGroup(group);
            childRepository.saveAndFlush(foundChild);
        }
    }

    private void saveOrUpdateAddress(Address address) {
        Address foundAddress = addressRepository.findByAddressId(address.getAddressId());
        if(foundAddress == null){
            addressRepository.saveAndFlush(address);
        }
        else{
            foundAddress.setStreetName(address.getStreetName());
            foundAddress.setHouseNumber(address.getHouseNumber());
            foundAddress.setFlatNumber(address.getFlatNumber());
            foundAddress.setPostalCode(address.getPostalCode());
            addressRepository.saveAndFlush(foundAddress);
        }
    }

    private String generateIdentifier(Child child){

        StringBuffer sb = new StringBuffer();
        sb.append(child.getName());
        sb.append(child.getSurname());
        sb.append(child.getId());

        String identifier = DigestUtils.md5DigestAsHex(sb.toString().getBytes());

        return identifier;
    }

}
