package pl.study.children;

import com.google.common.collect.Lists;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.study.model.Child;
import pl.study.model.RegistryEvent;
import pl.study.model.RegistryEvents;
import pl.study.register.RegistryEventType;
import pl.study.register.RegistryRepository;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.util.List;

@Controller
@RequestMapping("/rest")
public class ChildrenRestController {

    @Resource
    ChildRepository childRepository;
    @Resource
    RegistryRepository registryRepository;

    @RequestMapping(value = "getAllChildren", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    List<ChildDTO> getAllChildren() {

        List<Child> children = childRepository.getRawChildren();
        List<ChildDTO> rawChildren = Lists.newArrayList();
        for (Child child : children) {
            ChildDTO rawChild = new ChildDTO();
            rawChild.setId(child.getId());
            rawChild.setName(child.getName());
            rawChild.setSurname(child.getSurname());
            rawChild.setGroupName(child.getGroup().getName());
            rawChild.setIdentifier(child.getIdentifier());
            rawChildren.add(rawChild);
        }
        return rawChildren;
    }

    @RequestMapping(value = "registerChild/{identifier}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    void registerChild(@PathVariable String identifier) {

        Child child = childRepository.findByIdentifier(identifier);
        if (!child.isRegistered()) {
            RegistryEvent registryEvent = new RegistryEvent(child.getId(), RegistryEventType.REGISTER);
            registryRepository.save(registryEvent);
            child.setRegistered(true);
            childRepository.saveAndFlush(child);
        }
    }

    @RequestMapping(value = "unregisterChild/{identifier}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    void unregisterChild(@PathVariable String identifier) {

        Child child = childRepository.findByIdentifier(identifier);
        if (child.isRegistered()) {
            RegistryEvent registryEvent = new RegistryEvent(child.getId(), RegistryEventType.UNREGISTER);
            registryRepository.save(registryEvent);
            child.setRegistered(false);
            childRepository.saveAndFlush(child);

        }
    }

    @RequestMapping(value = "synchronizeRegistry", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public void synchronizeRegistry(@RequestBody RegistryEvents events) {

        for (RegistryEventDTO eventDTO : events.getRegistryEvents()) {
            Child child = childRepository.findById(eventDTO.getChildId());
            RegistryEventType eventType = RegistryEventType.valueOf(eventDTO.getEventType());
            if (RegistryEventType.REGISTER.equals(eventType)) {
                child.setRegistered(true);
            } else {
                child.setRegistered(false);
            }
            childRepository.saveAndFlush(child);
            RegistryEvent event = new RegistryEvent(eventDTO.getChildId(), eventType
                    , Timestamp.valueOf(eventDTO.getEventDate()));
            registryRepository.save(event);
        }
    }
}
